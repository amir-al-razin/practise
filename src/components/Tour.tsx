import React, { useState } from "react";

export default function Counter({ children, count: initialCount }) {
  const [count, setCount] = useState(initialCount);
  const add = () => setCount((i) => i + 1);
  const subtract = () => setCount((i) => i - 1);
  const icons = ["circle", "activity", "alert-triangle"];

  return (
    <>
      <section class="container flex-wrap flex flex-row items-start justify-center md:gap-3 px-4 mx-auto   max-h-[80vh] my-2 ">
        {icons.map((v, i) => (
          <div className="test"></div>
        ))}
      </section>
    </>
  );
}
