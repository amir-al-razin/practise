import React from "react";

interface Props {}
// reference science bee website / seofy wp theme
const NetworkT = (props: Props) => {
  return (
    <div className="lg:min-h-[80vh] md:flex hidden  flex-row items-center md:py-32 before:inset-x-0 min-h-screen relative before:bg-gradient-to-r before:from-transparent before:via-green-500 before:to-transparent  before:h-0.5  before:top-[50%] before:w-full  before:content-[''] before:absolute px-4">
      {new Array(4).fill(null).map((v, i) => (
        <div
          className={`flex items-center min-h-[10rem] justify-center  flex-col ${
            (i + 1) % 2 === 0 ? "flex-col-reverse pb-[2.75rem]" : "pt-12"
          }  `}
        >
          <h4 className={` whitespace-nowrap font-bold text-3xl `}>630</h4>
          <div
            className={`date-container  relative flex-col flex items-center  `}
          >
            <div className="relative my-4 hex">
              <div className="flex items-center w-5 h-5 bg-green-600 rounded-full animate-ping hexagon"></div>
              <div className="absolute top-0 w-5 h-5 transform scale-50 bg-green-600 rounded-full hexagon"></div>
            </div>
          </div>
          <div
            className={`content transform ${
              (i + 1) % 2 === 0 ? "md:-translate-y-8" : "md:translate-y-8"
            } rounded-primary relative  before:absolute bg-gray-800 before:left-0 before:top-0 py-1 px-6 before:content-[''] before:bg-green-500 before:w-full before:h-0.5 before:`}
          >
            <h5 className="text-lg font-semibold leading-loose ">
              Lorem ipsum dolor sit amet.
              <button>toast</button>
            </h5>
          </div>
        </div>
      ))}
    </div>
  );
};

export default NetworkT;
