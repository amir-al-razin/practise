import React, { useState } from "react";

export default function Footer() {
  const icons = [FaFacebook, FaGithub, AiFillInstagram, AiFillYoutube];

  const all_links = [
    {
      catagory: "Company",
      links: [
        {
          link: "Who we are",
          url: "/",
        },
        {
          link: "Privacy policy",
          url: "/",
        },
        {
          link: "Terms of use",
          url: "/",
        },
      ],
    },
    {
      catagory: "Crash Courses",
      links: [
        {
          link: "Who we are",
          url: "/",
        },
        {
          link: "Privacy policy",
          url: "/",
        },
        {
          link: "Terms of use",
          url: "/",
        },
      ],
    },
    {
      catagory: "Others",
      links: [
        {
          link: "Study",
          url: "/",
        },
        {
          link: "Skills",
          url: "/",
        },
      ],
    },
  ];

  return (
    <footer
      style={{
        backgroundImage:
          "linear-gradient(to right,#00ff40c0,transparent),url('https://www.iu.edu.sa/img/ium-blue-bg.jpg')",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
        clipPath: "polygon(30% 0, 100% 23%, 100% 100%, 0 100%, 0 19%)",
      }}
      className="relative bg-blend-multiply  min-h-[70vh] md:mt-8  pb-8 pt-[6rem] overflow-xs-hidden"
    >
      <div className="container grid min-h-[50vh] lg:grid-cols-5 mx-auto gap-x-4">
        <div className="flex flex-col items-start col-span-2 gap-4 px-6 py-4">
          {/* <img
            className="w-40"
            src="https://10minuteschool.com/assets/images/logo-white.png"
            alt=""
          /> */}
          <h1 className="text-3xl font-bold">Jc science society</h1>
          <div className="flex gap-4">
            {icons.map((I) => (
              <button className="p-2 bg-gray-800 shadow-lg rounded-primary">
                <I className="text-lg text-gray-100 " />
              </button>
            ))}
          </div>
          <p className="text-sm font-semibold">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis
            molestias quasi iste corrupti nostrum voluptatem ea consectetur
          </p>

          <img
            className="w-40"
            src="https://10minuteschool.com/assets/landing-page/playstore.svg"
            alt=""
          />
        </div>
        {all_links.map(({ catagory, links }, i) => (
          <div className="flex flex-col items-start gap-4 px-6 py-4">
            <h3 className="text-lg font-semibold">{catagory}</h3>
            <ul>
              {links.map(({ link, url }) => (
                <li>
                  <a className="text-sm font-semibold underline" href={url}>
                    {link}
                  </a>
                </li>
              ))}
            </ul>
          </div>
        ))}
      </div>
      <div className="w-4/5 xl:w-11/12 h-[.1rem] mx-auto bg-blue-200/50 mb-4 mt-2"></div>
      <p className="text-sm font-semibold text-center">
        Copyright © 2021 JC science society
        <br />
        Created with{" "}
        <img src="/assets/logo.svg" className="inline-block w-4 h-4" /> by{" "}
        <a href="http://github.com/amir-al-razin" className="text-yellow-500">
          Amir Razin
        </a>
      </p>
      {/* <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1440 320"
        className="absolute inset-x-0 bottom-0 h-full -z-5 "
      >
        <path
          fill="#0099ff"
          fill-opacity="1"
          d="M0,320L26.7,282.7C53.3,245,107,171,160,122.7C213.3,75,267,53,320,53.3C373.3,53,427,75,480,80C533.3,85,587,75,640,69.3C693.3,64,747,64,800,85.3C853.3,107,907,149,960,149.3C1013.3,149,1067,107,1120,122.7C1173.3,139,1227,213,1280,245.3C1333.3,277,1387,267,1413,261.3L1440,256L1440,320L1413.3,320C1386.7,320,1333,320,1280,320C1226.7,320,1173,320,1120,320C1066.7,320,1013,320,960,320C906.7,320,853,320,800,320C746.7,320,693,320,640,320C586.7,320,533,320,480,320C426.7,320,373,320,320,320C266.7,320,213,320,160,320C106.7,320,53,320,27,320L0,320Z"
        ></path>
      </svg> */}
    </footer>
  );
}

function FaGithub(props) {
  return (
    <svg
      stroke="currentColor"
      fill="currentColor"
      strokeWidth={0}
      viewBox="0 0 496 512"
      height="1em"
      width="1em"
      {...props}
    >
      <path d="M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z" />
    </svg>
  );
}

function FaFacebook(props) {
  return (
    <svg
      stroke="currentColor"
      fill="currentColor"
      strokeWidth={0}
      viewBox="0 0 512 512"
      height="1em"
      width="1em"
      {...props}
    >
      <path d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z" />
    </svg>
  );
}

function AiFillInstagram(props) {
  return (
    <svg
      stroke="currentColor"
      fill="currentColor"
      strokeWidth={0}
      viewBox="0 0 1024 1024"
      height="1em"
      width="1em"
      {...props}
    >
      <path d="M512 378.7c-73.4 0-133.3 59.9-133.3 133.3S438.6 645.3 512 645.3 645.3 585.4 645.3 512 585.4 378.7 512 378.7zM911.8 512c0-55.2.5-109.9-2.6-165-3.1-64-17.7-120.8-64.5-167.6-46.9-46.9-103.6-61.4-167.6-64.5-55.2-3.1-109.9-2.6-165-2.6-55.2 0-109.9-.5-165 2.6-64 3.1-120.8 17.7-167.6 64.5C132.6 226.3 118.1 283 115 347c-3.1 55.2-2.6 109.9-2.6 165s-.5 109.9 2.6 165c3.1 64 17.7 120.8 64.5 167.6 46.9 46.9 103.6 61.4 167.6 64.5 55.2 3.1 109.9 2.6 165 2.6 55.2 0 109.9.5 165-2.6 64-3.1 120.8-17.7 167.6-64.5 46.9-46.9 61.4-103.6 64.5-167.6 3.2-55.1 2.6-109.8 2.6-165zM512 717.1c-113.5 0-205.1-91.6-205.1-205.1S398.5 306.9 512 306.9 717.1 398.5 717.1 512 625.5 717.1 512 717.1zm213.5-370.7c-26.5 0-47.9-21.4-47.9-47.9s21.4-47.9 47.9-47.9 47.9 21.4 47.9 47.9a47.84 47.84 0 0 1-47.9 47.9z" />
    </svg>
  );
}

function AiFillYoutube(props) {
  return (
    <svg
      stroke="currentColor"
      fill="currentColor"
      strokeWidth={0}
      viewBox="0 0 1024 1024"
      height="1em"
      width="1em"
      {...props}
    >
      <path d="M941.3 296.1a112.3 112.3 0 0 0-79.2-79.3C792.2 198 512 198 512 198s-280.2 0-350.1 18.7A112.12 112.12 0 0 0 82.7 296C64 366 64 512 64 512s0 146 18.7 215.9c10.3 38.6 40.7 69 79.2 79.3C231.8 826 512 826 512 826s280.2 0 350.1-18.8c38.6-10.3 68.9-40.7 79.2-79.3C960 658 960 512 960 512s0-146-18.7-215.9zM423 646V378l232 133-232 135z" />
    </svg>
  );
}
