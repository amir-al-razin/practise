import React from "react";

interface Props {}
// reference science bee website / seofy wp theme
const Network = (props: Props) => {
  return (
    <div className="lg:min-h-[120vh] md:py-32 before:inset-y-0 min-h-screen relative before:bg-gradient-to-t before:from-transparent before:via-red-500 before:to-transparent  before:h-full  before:left-[50%] before:w-0.5  before:content-[''] before:absolute px-4">
      {new Array(6).fill(null).map((v, i) => (
        <div
          className={`flex items-center ${
            (i + 1) % 2 === 0 && "flex-row-reverse"
          } pb-8 `}
        >
          <div
            className={`date-container relative ${
              (i + 1) % 2 === 0
                ? "mr-[calc(50%-13px)] ml-[50px]"
                : "ml-[calc(50%-10px)] mr-[50px]"
            }  `}
          >
            <div className="flex items-center w-5 h-5 bg-pink-600 rounded-full animate-ping hexagon"></div>
            <div className="absolute top-0 w-5 h-5 transform scale-50 bg-pink-600 rounded-full hexagon"></div>
            <h4
              className={`absolute whitespace-nowrap font-semibold date ${
                (i + 1) % 2 === 0
                  ? "left-[100%] pl-[50px]"
                  : "right-[100%] pr-[50px]"
              } top-0 `}
            >
              8th,august,2021
            </h4>
          </div>
          <div className="content rounded-primary relative before:absolute bg-gray-800 before:left-0 before:top-0 py-4 px-8 before:content-[''] before:bg-pink-500 before:w-0.5 before:h-full before:">
            <h5 className="mb-2 text-lg font-semibold leading-loose">
              Lorem ipsum dolor sit amet.
            </h5>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Network;
