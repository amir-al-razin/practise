module.exports = {
  mode: "jit",
  purge: ["./public/**/*.html", "./src/**/*.{astro,js,jsx,svelte,ts,tsx,vue}"],
  darkMode: "media", // or 'media' or 'class'
  theme: {
    borderRadius: {
      sm: "3px",
      primary: "5px",
      md: "0.375rem",
      lg: "0.5rem",
      full: "9999px",
    },
    boxShadow: {
      t: "box-shadow: 0px 7px 4px 0px #FF000040",
    },
    fontFamily: {
      primary: ["sans-serif"],
      bubble: ["Bubblegum Sans", "sans-serif"],
    },
    gradientColorStops: (theme) => ({
      ...theme("colors"),
      bg: "#111827",
    }),
    color: {},
    minHeight: {
      "4/5": "80vh",
      screen: "100vh",
    },
    maxWidth: {
      "1/2": "50%",
    },
    extend: {
      zIndex: {
        "-10": "-10",
        "-5": "-5",
      },
      backgroundImage: (theme) => ({
        hero: "url('/assets/banner.png')",
      }),
      inset: {
        "-1/5": "-20%",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
